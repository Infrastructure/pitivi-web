---
title: 'June 2014 development update'
date: Mon, 23 Jun 2014 21:24:00 +0000
---

Good news everyone !

This is the first blog post of a series of updates about our latest development efforts in GStreamer / gst-editing-services / Pitivi.

This post's focus will be on [MPEG transport stream](http://en.wikipedia.org/wiki/MPEG_transport_stream), a format now nearly twenty years old, originally developed and still widely used for and by the broadcasting industry. In the mid-2000s, [some people](http://www.avchd-info.org/) decided it would be a great idea to use this format in camcorders, stuffed a rather useless timestamp in there for good measure and started to ship AVCHD camcorders like it was the greatest thing since sliced bread.

It was not. See, most modern video codecs such as h264 rely on the notion of keyframes: to compress video streams as much as possible, most frames are encoded as their difference with the previous frame, we call these frames delta units. Sparsely distributed in the encoded stream are keyframes. These frames can be decoded without any reference to past frames, and when trying to seek in such a stream, decoding has to start from a keyframe and progress through all the delta units up to the requested frame.

Video editing implies accurate seeking, for example if you only want to include the 10 last frames of a 2-hour clip, decoding the whole file to obtain these few frames would be a pointless waste of time.

Failing to start decoding from a keyframe when seeking creates artefacts, garbled frames : the decoder is missing information to decode the delta unit, tries to provide a frame nevertheless and fails in doing so, until the next keyframe has been reached. Containers that are readily usable for editing contain information about the location of keyframes, in one form or another. This is not the case of MPEG TS, of which AVCHD is a subset. Locating the keyframes thus becomes a rather involved process, as one needs to parse the video streams in order to do so.

Backtracking to the introduction of this post, good news everyone ! We just [did that](https://bugzilla.gnome.org/show_bug.cgi?id=675132), and here is a before / after video to demonstrate our changes. We can now ensure full support of AVCHD, enjoy :D

The next two posts will be respectively focused on our refactoring of our video / audio mixing stack, and our ongoing work on gnonlin, our non-linear editing engine.