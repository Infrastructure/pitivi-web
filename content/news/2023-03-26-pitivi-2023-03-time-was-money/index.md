---
title: Pitivi 2023.03 — Time Was Money
date: Sun, 12 Aug 2023 12:28:00 +0000
tags:
  - Release
summary: Auto alignment and precise waveforms.
---

The Pitivi team proudly presents Pitivi version **2023.03** "Time was money".

What's new
----------

Shout out to Thejas Kiran who enhanced the Timeline during GSoC 2022. In the second part of his internship, he resurrected the auto aligner we lost previously.

{{< figure src="autoaligner.png" title="Pitivi autoaligner is back" >}}

In the process, Thejas discovered the audio previewers can be improved and now they look much better.

{{< figure src="audio-waveforms.png" title="Pitivi audio waveforms, new and old" >}}

Quality
-------

A bunch of fixes and unit-tests have been contributed, most by awesome GSoC students. Please see the corresponding [2023.03 milestone](https://gitlab.gnome.org/GNOME/pitivi/-/milestones/10) for some of those.

Download
--------

You should be able to upgrade to Pitivi 2023.03 when we update [flathub](https://flathub.org/apps/details/org.pitivi.Pitivi). There is a high chance the rolling distros pick it up faster.

What's next
-----------

Aryan Kaushik ported Pitivi from GTK3 to GTK4 during GSoC 2022. As the task is huge, there are still a few things to do. Sneak peek: Jainil will continue the porting as part of GSoC 2023. Once done, we make a release to get it out of the way.

Help wanted
-----------

There are many things to improve and polish, come have a chat if you know Python or Rust!
