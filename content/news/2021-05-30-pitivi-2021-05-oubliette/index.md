---
title: Pitivi 2021.05 — Oubliette
date: Sun, 30 May 2021 07:00:00 +0000
tags:
  - Release
summary: Media Library clips tagging.
---

The Pitivi team remembered to present Pitivi version **2021.05** "Oubliette".

New features
------------

Clips in the Media Library can now be tagged and filtered by tags. Abhishek Kumar Singh initially refactored the Media Library to simplify and cleanup the code. The buttons which act on the selected clips have been moved at the bottom of the Media Library so they can be identified more easily. We hope you enjoy the sleek Media Library UI!

This allowed, in the second half of his Google Summer of Code internship, to develop the tagging of clips in the Media Library. It's also possible to tag multiple clips at once.

{{< figure src="media-library-clips-tagging.png" title="Pitivi tagging of clips in the Media Library" >}}

Searching for clips tagged with "manifestation", for example, is done by writing "tag:manifestation" in the Media Library search box. Come to our chat room and tell us how else we can put tags to good use!

The timeline also got two small improvements. It's now possible to move keyframes vertically or horizontally by dragging them while keeping Ctrl pressed. To seek to the previous/next marker you can now use keyboard shortcuts. Click the timeline, press `/`, then type "marker" to discover the actual keyboard shortcuts.
