---
title: 'Pitivi Fundraiser Week One Update (And A Great Piece Of News)'
date: Mon, 03 Mar 2014 21:28:00 +0000
tags:
  - Fundraiser
---

**Greetings Pitivi supporters!**
================================

We hope everyone had a great week! We've had a rather hectic one, and hopefully that's just the beginning. This is the first update for our fundraising [campaign](http://fundraiser.pitivi.org/), be sure to check our blog weekly for more ;)

Announcement!
-------------

We are happy to announce that the GStreamer maintainers decided to show us their faith and support, by allocating 2 500 € to our project from GStreamer funds! This is great news for several reasons:

*   It's obviously nice to get such an amount of money as it represents **seven percent** of the total needed to get a 1.0 release or, to put it another way, **three weeks** of full time development!
*   GStreamer is the central component of our architecture, and it is the one on which we plan to spend most of our time during the push to 1.0. Pitivi really is just the tip of the iceberg, to put things in perspective, it now only is a mere 25,000 lines of Python code, whereas GStreamer and its plugins represent around 1.5 _million_ lines of code.Our work really benefits every other project that uses GStreamer (for example, accurate seeking in ogg files? That was [us](https://bugzilla.gnome.org/show_bug.cgi?id=700537)!), and it is meaningful to see the GStreamer maintainers acknowledge that "officially", many thanks to them! And then many more for the road. They're awesome and a big reason why we love working on Pitivi.
*   We really hope this donation will help everyone that cares about Open Source, be they individuals craving for flawless multimedia handling on Linux or companies interested in building products around GStreamer, to see that we are an integral part of the community, and that donating to the campaign is not only about getting a great video editor, but also about improving the core multimedia engine shared by most if not all the Linux distributions!

Acknowledgement!
----------------

We would like to thank each and everyone of the 350+ backers that already donated to the campaign and helped us break the 10 000 euros bar during this last week. 11 000 € is a great amount of money, sufficient to cover our expenses for **three months** of full-time development! With your help, we already made it to a third of our first goal, and with your help we can make it to Pitivi 1.0 and beyond. Anything helps, be it blogging, tweeting and sharing on social networks, or getting the word out to journalists. DistroWatch also decided to make [us](http://distrowatch.com/weekly.php?issue=20140303#donation) the recipient of their monthly donation, and granted us 280 euros, it's a great honor for us to be listed among the previous recipients of that donation!

Appetizement!
-------------

The dictionary doesn't seem to agree that this word should exist, but it's here nevertheless. Next week should see an interesting announcement for all the fans of Python, Romania and clean code, make sure to stay tuned on our [twitter](https://twitter.com/Pitivi) or to add this blog to your RSS feeds ;)