---
title: 'First delivery of the Pitivi fundraiser: universal daily bundles for Linux'
date: Mon, 14 Apr 2014 21:26:00 +0000
tags:
  - Fundraiser
---

The Pitivi community is very happy to announce the availability of easy to use, distro-independent Linux bundles to test latest version of the application. This eliminates dependency problems and allows quicker testing cycles. Our entire [stack](https://developer.pitivi.org/Architecture.html) is bundled, so the only requirement is glibc ≥ 2.13.

Simply [Download the bundle](http://fundraiser.pitivi.org/download-bundles) and run it!

This is the first delivery of the Pitivi Fundraiser—as you can see, we are already well on our way to deliver what has been promised in our [detailed planning](http://fundraiser.pitivi.org/the-plan). You can have a look at what is happening with the "daily build" bundles on [on our Jenkins instance](https://jenkins.arracacha.collabora.co.uk/view/pitivi/job/pitivi-bundling/) (main server hosting donated by Collabora—thanks!).

To build the bundles we use [Cerbero](http://docs.gstreamer.com/display/GstSDK/Multiplatform+deployment+using+Cerbero), which is the build and packaging system used by Collabora and Fluendo to construct the [GStreamer SDK](https://gstreamer.freedesktop.org/), and is also used by the GStreamer community to deliver [GStreamer 1.x binaries for Android, iOS, Mac OS X and Windows](https://gstreamer.freedesktop.org/data/pkg/). It is a very useful and well-designed technology, which will allow us to easily create packages for Windows and Mac OS X in the future.

This does not only apply to us, of course: work that has been made for creating Linux distro bundles allows anyone to easily create bundles for their applications with Cerbero. This has not been merged just yet, but that should happen quite soon. If you want to bundle your app using Cerbero, do not hesitate to ask us, even if it should already be **really** straight forward!

We raised half of the amount we are targeting for the Pitivi fundraising campaign, and we are in very good shape to be able to deliver everything on time. We need your help to reach that target.

Please [donate now](http://fundraiser.pitivi.org/donate) to make sure we will be able to provide the community with the great video editing app it deserves!