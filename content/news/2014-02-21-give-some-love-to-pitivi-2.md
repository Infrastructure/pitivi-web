---
title: 'Give some love to Pitivi !'
date: Fri, 21 Feb 2014 21:30:00 +0000
tags:
  - Fundraiser
---

Today we're thrilled to announce a [crowdfunding](http://fundraiser.pitivi.org/) campaign to support the development of Pitivi!

We have made the choice not to use one of the major crowdfunding platforms such as kickstarter for [multiple reasons](http://fundraiser.pitivi.org/faq), and instead partner with the [GNOME foundation](http://www.gnome.org/foundation/), which is ideologically aligned with us and will support our financial infrastructure.

We are proud of that partnership, as we share their objective of "creating a free software computing platform for the general public", and the foundation is excited as well:

> "GNOME is proud to host the Pitivi campaign. Pitivi fills a real need for stable and approachable high quality video editing. Its software architecture and UI design share the same sleek and forward-thinking approach that we value in the GNOME project." — _Karen Sandler, executive director of the GNOME Foundation_

With that campaign, our aim is to provide everyone with a rock-solid Free Software video editor, based on the awesome technology that is GStreamer. We have spent a lot of time working on the campaign website, and it holds a lot more content than a simple blogpost could.

We know that what we want to do is the right thing, and requesting money for quality and stabilization first is the correct and honest thing to do. We obviously encourage you to [donate](http://fundraiser.pitivi.org/donate) to the campaign, but we also hope that you will be willing to spread the message, and explain why what we do is important and good.

Free and Open Source video editing is something that can help make the world a better place, as it gives people all around the world one more tool to express themselves creatively, fight oppression, create happiness and spread love.

Hoping you'll spread the love too, thanks for reading!