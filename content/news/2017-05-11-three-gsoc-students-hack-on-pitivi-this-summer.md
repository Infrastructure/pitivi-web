---
title: 'Three GSoC students hack on Pitivi this summer'
date: Thu, 11 May 2017 22:58:34 +0000
tags:
  - GSoC
---

We're excited to have three students that will contribute to Pitivi this summer. Congratulations, Fabián Orccón, Suhas Nayak and Ștefan-Adrian Popa!

*   [Fabián](http://teetux.com/) already had an internship with us two years ago. He'll focus on a plugin system with great documentation for video hackers, and a first plugin for inspecting the timeline of the currently opened project in a Python console ([T3193](https://phabricator.freedesktop.org/T3193)).
*   [Suhas](https://suhas2go.github.io) will focus on a modern color correction UI for the corresponding GStreamer plugin, and lay the path for similar changes to other effects ([T2372](https://phabricator.freedesktop.org/T2372)).
*   [Ștefan](https://stefanpopablog.wordpress.com) will focus on allowing users to apply [Ken-Burns effects](https://en.wikipedia.org/wiki/Ken_Burns_effect) by manipulating the placement and zoom of the clips on the viewer ([T7340](https://phabricator.freedesktop.org/T7340)).

You can follow the linked blogs or subscribe to the Phabricator tasks to stay up-to-date with the progress, or keep an eye on the [Pitivi planet](https://www.pitivi.org/planet/). Feel free to make suggestions about these projects on the corresponding tasks, or chat with us about them in our [IRC channel](https://www.pitivi.org/contact/). The official start of the coding period is May 30.

Besides mentoring for GSoC, the Pitivi maintainers are busy ironing out the last stability problems we're aware of. We made great progress, expect version 0.99 sometime soon, followed by some more testing towards 1.0!

Thanks to Google for [GSoC](https://developers.google.com/open-source/gsoc/) and to the [GNOME Foundation](https://www.gnome.org) for allowing us to have these projects under their umbrella this year!