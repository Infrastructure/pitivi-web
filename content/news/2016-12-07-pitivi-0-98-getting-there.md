---
title: 'Pitivi 0.98 — Getting there'
date: Wed, 07 Dec 2016 17:37:08 +0000
tags:
  - Release
---

This is another [release](http://wiki.pitivi.org/wiki/0.98) focused on fixing bugs and improving stability.

Improved timeline
-----------------

We switched our [official build](http://wiki.pitivi.org/wiki/Install_with_flatpak) to use GTK 3.22 and the framework did not like how we were using it, spamming us with warnings in the console. We fixed those and improved the timeline in the process and added more unit tests.

> It was [quite a journey](https://phabricator.freedesktop.org/T7573). Initially, the GTK Inspector was useful to figure out which widgets the widget IDs in the warnings identified. Then we had to go back between the GTK documentation and the #gtk+ IRC channel until we figured out we were changing the size of some containers in their do\_draw methods, which was not good.

Accelerated development
-----------------------

Taking advantage of an opportunity, with the last money from our [coffers](https://www.pitivi.org/donators/) (the remaining 2014 fundraiser donations and the GSoC mentor stipends) we hired Alexandru Băluț, a long-time Pitivi contributor, so he can focus better on fixing issues blocking the 1.0 release. Alex worked on Pitivi in Oct, Nov, and will allocate more time in Dec 2016. Thanks again to everybody who donated!

Customizable keyboard shortcuts
-------------------------------

Jakub Brindza, our GSoC 2016 student, finished the [customizable keyboard shortcuts](http://www.jakubbrindza.com/2016/08/gsoc-with-pitivi.html) feature. See how it works in the screencast below.

{{< youtube RSUOFEpHpH4 >}}

Supported muxers and encoders
-----------------------------

In the previous version, 0.97, we picked a set of [supported muxers, audio encoders, video encoders](https://github.com/pitivi/pitivi/blob/0ae789140bd28f96e91fb53c4df2d7168473f665/pitivi/render.py#L91), added integration tests for them in GES, and changed the rendering dialog to show very clearly the ones which are unsupported.

1.0
---

See what's left to do for 1.0 in the [0.99 and 1.0 columns in Phabricator](https://phabricator.freedesktop.org/tag/pitivi/). If you want to [help](https://www.pitivi.org/contributing/) in any way, come to our [IRC channel](https://www.pitivi.org/contact/). We prepared a nice [list of tasks suitable for newcomers.](https://phabricator.freedesktop.org/project/view/111/) These are good also for students interested to [apply for a GSoC with us](http://wiki.pitivi.org/wiki/Google_Summer_of_Code).

The build system has been ported to [meson](http://mesonbuild.com/), no more autogen! ;)