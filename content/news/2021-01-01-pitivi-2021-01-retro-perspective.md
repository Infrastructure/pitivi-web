---
title: Pitivi 2021.01 — Retro Perspective
date: Mon, 01 Feb 2021 07:00:00 +0000
tags:
  - Release
summary: Bug fixes.
---

The Pitivi team retroactively presents Pitivi version **2021.01** "Retro Perspective". This release includes a handful of bug fixes.

That's all?
-----------

We did promise to release more often. What did you expect?

Under the hood
--------------

Please see the corresponding [2021.01 milestone](https://gitlab.gnome.org/GNOME/pitivi/-/milestones/7) for details.

Thanks
------

Many thanks to all the contributors!