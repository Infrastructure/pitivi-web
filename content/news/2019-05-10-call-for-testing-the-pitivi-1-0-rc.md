---
title: 'Call for testing the Pitivi 1.0 RC'
date: Fri, 10 May 2019 06:58:47 +0000
---

Pitivi 1.0 is scheduled to be released on Monday, May 20th. All the important bugs we were aware of have been fixed.

To fix one of the last issues, Thibault very recently rewrote the timeline/layers/clips management in GES, and this might have introduced new bugs. While we have lots of tests which all pass, they don't cover everything.

We ask you to test the 1.0 RC! Grab a bunch of video files shot with your phone or camera and make some video out of them, trying various features. Tell us if you notice problems, and if you can reproduce them please file an issue describing the steps to reproduce the bug.

You can find us in the [Pitivi Matrix room](https://matrix.to/#/#pitivi:matrix.org).

How to install
--------------

To install the Pitivi 1.0 RC, simply run:

```
flatpak install http://flatpak.pitivi.org/pitivi.flatpakref
```

If there are conflicts, you can uninstall the conflicting installation.

How to test
-----------

Start Pitivi from the console, and keep it in view to notice any warnings or errors which might show up:
```
flatpak run org.pitivi.Pitivi//stable
```

You should be able to use any video file supported by GStreamer, but we officially support only a limited set of formats. If the file appears in the media library with a warning sign, right-click it and select proxy to create an optimized media file used automatically instead of the original.

Most useful would be to test the timeline editing, which should be responsive and precise. Try for example [splitting](https://www.pitivi.org/manual/splitting.html) and [trimming](https://www.pitivi.org/manual/trimming.html) clips, moving them around. Try ungrouping audio-video clips to make the respective video shorter than the audio. Overlap clips to create cross-fade [transitions](https://www.pitivi.org/manual/transitions.html).

If time allows, you can also try adding effects, for example adding a background to a green screen. Mix diverse footage formats in the same timeline. Make a slideshow and include title clips and background music.

For reference, see the [user manual](https://www.pitivi.org/manual/). At the end, please render it and show us what you did!