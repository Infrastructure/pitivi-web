---
title: 'Migrating to GNOME''s GitLab'
date: Tue, 27 Feb 2018 13:03:36 +0000
---

Three years ago we [switched](http://blog.aleb.ro/2015/10/pitivi-moves-from-bugzilla-to.html) our bug tracker from Bugzilla to Freedesktop's Phabricator instance. As very few projects were using it, the maintenance cost was too high for the gain, so the current plan is to obsolete it. Phabricator worked well for us, but now we say bye.

Luckily for us, GNOME hosts a GitLab instance since last year. We just migrated the Phabricator tasks to it and now we're at [https://gitlab.gnome.org/GNOME/pitivi](https://gitlab.gnome.org/GNOME/pitivi).

This was possible thanks to [Thibault](https://wiki.gnome.org/ThibaultSaunier) for extending the [bztogl](https://gitlab.gnome.org/Incubator/bztogl) tool used to migrate the tasks, to [Carlos](https://wiki.gnome.org/CarlosSoriano) for carefully running it, and to the GNOME community for everything.

We'll certainly benefit a lot from the tighter integration with GNOME. This makes it much easier for GNOME contributors of all kinds to take part in our awesome video editor.