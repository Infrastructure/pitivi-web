---
title: 'Get Pitivi directly from us with Flatpak'
date: Mon, 18 Jul 2016 22:05:45 +0000
---

Distributing apps as packages (deb, rpm, etc) is problematic. For example, the Pitivi package depends on the GTK package and Pitivi 0.95 [broke](https://bugs.archlinux.org/task/49301) in the distributions which updated to GTK version 3.20, because of the incorrect way we were using a virtual method. This is not the first time something like this happens. To avoid the slippery dependencies problem, two years ago we started making [universal daily builds](https://pitivi.wordpress.com/2014/04/14/first-delivery-of-the-pitivi-fundraiser-universal-daily-bundles-for-linux/).They allowed everybody to run the latest Pitivi easily by downloading a large binary containing the app and all the dependencies.

A few problems still remained:

* It was complicated to set up the Pitivi development environment. We had a huge script for setting up the development environment and new contributors always had problems. We tried to reuse the daily builds for preparing a development environment but it was hacky and we failed to make it work correctly.
* Users were notified about available updates through a custom mechanism and updating was not ideal, requiring to download a huge binary blob.
* Maintaining the AppImageKit based bundle required following many dependencies (security) updates and maintaining the build for them (even though we were sharing the burden with the whole GStreamer community, as we were using the [Cerbero](https://cgit.freedesktop.org/gstreamer/cerbero/) build system).

Recently [Flatpak](http://flatpak.org/) got our attention as it set out to fix these problems. Flatpak’s sandboxing allowed us to create a nice development environment. Our new [dev env script](https://git.gnome.org/browse/pitivi/tree/bin/pitivi-env) reuses [the one which flatpaks Pitivi](https://git.gnome.org/browse/pitivi/tree/build/flatpak/pitivi-flatpak) so we [got rid of the old ones](https://git.gnome.org/browse/pitivi/commit/bin?id=603f52aed54fa1d7100894ecd403776fe7e02cc6). Also, Flatpak allows the user to only download binary diffs for updates. Moreover, the Gnome community already provides builds for their SDK and Runtimes, which contains almost everything we need for Pitivi; now, we only have to build a [few libraries](https://phabricator.freedesktop.org/diffusion/PTV/browse/master/build/flatpak/pitivi.template.json;9dc539717e1ac04d159f9276d2990f82d57c6c25), which makes it much simpler for us to maintain the new bundles.

We're enthusiastic about Flatpak. Jakub Steiner [summarized](http://jimmac.musichall.cz/blog/2016-05-23-year-of-the-linux-desktop/) Flatpak's advantages very nicely:

> Flatpak aims to solve the painful problem of the Linux distribution—the fact that the OS is intertwined with the applications. It is a pain to decouple the two to be able to:
> 
> *   Keep a particular version of an app around (and working, our note), regardless of OS updates. Or vice versa, be able to run an uptodate application on an older OS.
> *   Allow application authors distribute binaries they built themselves. Binaries they can support and accept useful bug reports for. Binaries they can keep updated.

For now you need to use the command line to [install Pitivi with Flatpak](http://wiki.pitivi.org/wiki/Install_with_flatpak). In the near future, it should be trivial for you to install and manage our Flatpak build of Pitivi in your favorite software manager. GNOME Software is a [good example](https://blogs.gnome.org/hughsie/2016/07/05/flatpak-and-gnome-software/).