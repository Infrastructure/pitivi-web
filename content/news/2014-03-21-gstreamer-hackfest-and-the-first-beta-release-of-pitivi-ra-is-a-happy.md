---
title: 'GStreamer Hackfest and the first Beta release of Pitivi, "Ra is a happy"'
date: Fri, 21 Mar 2014 21:27:00 +0000
tags:
  - Release
---

Last week-end, part of the Pitivi Team went to the GStreamer Hackfest in Google's offices in Munich to work with twenty other GStreamer hackers on various important technical issues. A big thanks to Google and Stefan Sauer for hosting the event! Keep your eyes peeled: we will soon blog the results of the work the Pitivi team has accomplished during the hackfest.

During the hackfest a very important milestone has been reached: the first [GStreamer Editing Services](http://gstreamer.freedesktop.org/releases/gst-editing-services/1.2.0.html), [GNonLin](http://gstreamer.freedesktop.org/releases/gnonlin/1.2.0.html) and [gst-python](http://gstreamer.freedesktop.org/releases/gst-python/1.2.0.html) stable versions in the 1.X branch have been released. That means that these very central components of the Pitivi project are now considered stable.

While this backend work was essential to the [beta release](http://wiki.pitivi.org/wiki/0.93), we also want to specifically thank Alexandru Balut for his impressive involvement during the 0.92 -> 0.93 cycle. He provided an impressive amount of bug fixing and cleanup patches in Pitivi itself, and has greatly helped the project reach a beta state. Any inquiries regarding the 0.93 release codename must be sent in his general direction.

This release will be the basis on which we will start our work for our ongoing [fundraiser](http://fundraiser.pitivi.org/). We've done that work in our spare time, and we're excited about what we'll be able to accomplish once we start working full time! Thibault Saunier has already been preparing bundles for the release, more on that in the next post!

Once again, you can [help](http://fundraiser.pitivi.org/donate) us in producing a rock solid stable release, by donating and spreading the message!