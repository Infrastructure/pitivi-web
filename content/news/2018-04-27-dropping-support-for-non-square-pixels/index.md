---
title: 'Dropping support for non-square pixels in Pitivi'
date: Fri, 27 Apr 2018 20:06:28 +0000
---

Emerging from the long history of the video broadcast industry, there are legacy standards which specify _rectangular_ pixels instead of square pixels. Yes, really! According to [Wikipedia](https://en.wikipedia.org/wiki/Pixel_aspect_ratio), non-square pixels originate in early digital TV standards:

> The term _pixel aspect ratio_ was first coined when [ITU-R BT.601](https://en.wikipedia.org/wiki/Rec._601 "Rec. 601") (commonly known as "[Rec. 601](https://en.wikipedia.org/wiki/Rec._601 "Rec. 601")") specified that standard-definition television pictures are made of lines of exactly 720 non-square pixels.

Today, we're announcing that we will no longer support rendering non-square pixels. It will still be possible to use videos with non-square pixels in your projects, though.

Pitivi allowed creating projects with non-square pixels, but there were quality and behavior issues, such as this [inconsistent viewer and renderer behavior](https://gitlab.gnome.org/GNOME/pitivi/issues/1406#note_70802) related to them.

GStreamer Editing Services (GES), the library used by Pitivi for video processing, is very flexible and allows using videos of any video format in the same project. However, normally, in a "pro" setup, most video editing applications are very strict about the formats they accept as input, so Pitivi and GES were a bit unconventional with the "anything goes" approach.

This might give a clue as to why it was rather complicated and not very rewarding to "properly" fix the rectangular pixels problems in GES. The following question arose in our IRC channel (#pitivi on freenode):

> \[2017-10-30 14:37:38\] &lt;thiblahute&gt; nekohayo: So my main question would be, do we really care these days to be able to produce non square output?

And we mostly came to the conclusion that only dinosaurs are producing content with rectangular pixels, and that with our limited resources we can't afford to spend time and effort on this:

> \[2017-10-30 14:44:06\] &lt;nekohayo&gt; so... it seems that non-square only matters for older formats
> 
> \[2017-10-30 14:44:45\] &lt;nekohayo&gt; and since we gave up on handling DV tapes/firewire anyway... we could as well declare PAR dead. Because f\*\*\* the 1990-2000's
> 
> \[2017-10-30 14:45:29\] &lt;nekohayo&gt; Mathieu\_Du's only got love for you if you were born in the 80's
> 
> \[2017-10-30 14:46:58\] &lt;Mathieu\_Du&gt; the what again ?
> 
> \[2017-10-30 16:36:38\] &lt;nekohayo&gt; pixel aspect ratio ([acceptable in the 80's](https://www.youtube.com/watch?v=Ckff_bkA5z8))
> 
> \[2017-10-30 16:37:28\] &lt;Mathieu\_Du&gt; aha, knew that song from lubosz though :)

As a result of this simplification, the aspect ratio controls in the project settings dialog have been removed—one less thing for the user to worry about—and so the project width and height are now the only settings defining the display aspect ratio.

{{< figure src="video_settings2.jpg" title="The aspect ratio settings removed from the project settings dialog" >}}
