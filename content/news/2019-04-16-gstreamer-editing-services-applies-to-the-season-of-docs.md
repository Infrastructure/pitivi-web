---
title: 'GStreamer Editing Services applies to the Season of Docs'
date: Tue, 16 Apr 2019 07:06:28 +0000
tags:
  - Season of Docs
---

The Pitivi video editor is [based](https://developer.pitivi.org/GES.html) on the [GStreamer Editing Services](https://gstreamer.freedesktop.org/documentation/gst-editing-services/index.html) library. GES makes it easy to manage the timeline of a video project and export it to a new video file, and is carefully built to be reusable by other projects, not only Pitivi.

Since a few years ago, while not [mentoring](https://developer.pitivi.org/Past_GSoCs.html) students for GSoC, we've been busy working on Pitivi 1.0, about to be released. A large part of this was spent on fixing and improving the GES library. Time has come for the GES documentation to also be improved, to attract new users and contributors to the GStreamer ecosystem.

We're applying to the [Season of Docs](https://developers.google.com/season-of-docs/) program, where Google pays technical writers to contribute to open-source projects. Check out the [technical writer guide](https://developers.google.com/season-of-docs/docs/tech-writer-guide) for details and the program timeline, and read below if you are interested in working with us!

**UPDATE**: We've been accepted, see the [list of accepted open-source organizations](https://developers.google.com/season-of-docs/docs/participants/).

![SeasonofDocs_Logo_SecondaryGrey](/i/seasonofdocs_logo_secondarygrey.png)

Project ideas
-------------

As GES is a relatively small project, we have a single project idea, composed of multiple smaller tasks.

### Write overviews and clarify the API reference

What GES is missing the most is a set of easy to understand high-level **overviews** for newcomers. You could write for example overviews about:

*   Assets and proxy management in a project
*   Timeline composed of Layers, and the output a/v tracks
*   Layers composed of various Clip types, and how these can be created
*   Effects which can be applied to Clips, and controllable properties

While preparing the high-level overviews you'll dive into the **API reference**. The current API documentation is usable, but is not in the best shape. As you notice gaps, unclear sentences, missing details, you would either fix them on the spot or take note and fix them later. The entire API reference can then be reviewed and refreshed in this second step.

A stretch goal would be writing one or more **tutorials**, to exemplify easy video tasks which can be automated. The base code for these would be provided by us, and be most probably written in Python:

*   Cutting every third second of a video to produce a shorter video with skips,
*   Creating a photos slideshow with random transitions between the photos,
*   Displaying multiple videos at the same time in mosaics,
*   Animating a title clip in the center of the image from 0% to full size.

The GES overviews and tutorials would be kept in the [GES git repository](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/docs) as markdown files. The API is documented in the [source code](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/ges) written in C.

To get a better idea about the GES API, check the [hierarchy of classes](http://lazka.github.io/pgi-docs/#GES-1.0/hierarchy.html).

The current [GES documentation](https://gstreamer.freedesktop.org/documentation/gst-editing-services/index.html) is integrated into the GStreamer documentation. The structure is specified in [docs/sitemap.txt](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/blob/master/docs/sitemap.txt), and the HTML is generated with [Hotdoc](https://hotdoc.github.io) out of the C files and [other markdown bits](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/docs).

If interested, [get in touch](https://www.pitivi.org/contact/) with us as soon as possible, so we can prepare.