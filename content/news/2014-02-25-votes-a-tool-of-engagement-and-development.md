---
title: 'Votes: a tool of engagement and development.'
date: Tue, 25 Feb 2014 21:29:00 +0000
tags:
  - Fundraiser
---

During the creation of the [campaign](http://fundraiser.pitivi.org/), we debated what kind of perks we should offer. The thing is, we are not t-shirt creators, we are software developers and UI designers.

We believe people who give us money do so in order for us to develop a **good software**, and thus we tried to focus on perks that made real sense. What could we offer to the community that would help us in making the software that they truly want? Our answer to that question: _a voice_, simple as that.

Though we already have a very active community and listen to feedback from our users, we were missing a way to quantify the priority of feature requests for the people to whom our software matters enough to sponsor our work.

We decided on two types of perks we would offer: invitations to hangouts organized monthly (we'll tell you more about that in a future post), and the subject of today's post, our voting system!

We wanted to grant the possibility of voting to anyone who donated, from the lowest amount available, and decided to weigh the vote proportionally to the amount given, with steps at which the curve flattens a bit, to make sure people who can't donate 300 euros still have a reasonable chance to have their voice heard.

Later I'll post implementation details for those interested, on my personal blog. Suffice to say that it works, and more exciting that people are already making good use of it!

This brings me to the exciting news we want to share: we made the current vote results public! Obviously, as we're in the early stages of the campaign, they're not nearly as significant as they will be later, but we think it's already interesting data, and you can have a look at them live right [now](http://fundraiser.pitivi.org/vote/results/).

Now to our own analysis of the data so far:

The first exciting figure for us is that even though we're not yet guaranteed to reach a funding point that can put vote results into development (you can help us get there [faster](http://fundraiser.pitivi.org/donate)), one third of our backers already took the time to go through the form and rate features on the 0 to 10 scale, and we clearly expect that ratio to grow if we reach the 35,000 bar.

We interpret that as a sign that our voting system answers a real demand. This figure is a clear success in our effort to create sustainable community engagement to support responsive and dynamic Pitivi development, alongside the growing number of people who choose to put their faith in us and donate.

Some points in the feature ranking results caught our attention:

*   The clear first place of hardware accelerated decoding and encoding. This is really interesting to the engineers among us, who already salivate at the possible prospect of implementing it!It also goes to show that performance is critical to people using video editing software, and reassures us in our architectural choices: the decision to ally with GStreamer means a lot of the heavy lifting is done as part of a partner project that doesn't have to be written by Pitivi developers from scratch. Instead, we contribute to GStreamer while also reaping the huge benefits of it -- and that means we can focus better on the video editing side of our code (making sure dynamic pipelines work with hardware-accelerated decoders/encoders, adapting and extending our integration test suites to ensure it keeps on being true)
*   The very pragmatic second place of copy paste, a small but oh-so-helpful feature, which goes to show that our backers are sensible, productivity and detail-oriented people.
*   The low ranking of Windows and Mac ports, which is certainly due in part to the fact that awareness about our campaign is pretty much limited to the Free Software community for now.
*   Finally, something we don't really know how to interpret on the spot, but that is interesting to remark nevertheless is that the three last spots are occupied at the time of writing by external project formatters, such as Final Cut projects.

I'll repeat that these rankings are absolutely not definitive, as already existing backers can change their votes and new backers should hopefully continue to give their opinion on what matters to them.

The conclusion is: "Don't like these rankings ? [Donate](http://fundraiser.pitivi.org/donate) and you can contribute in changing them!"

We're really interested in your analysis of these early results, and hoping that discussion will occur about them in the comments section or on our IRC channel (#pitivi on freenode)!

Thanks for reading, the Pitivi team