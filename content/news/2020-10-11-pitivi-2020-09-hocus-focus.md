---
title: Pitivi 2020.09 — Hocus Focus
date: Sun, 11 Oct 2020 14:05:54 +0000
tags:
  - Release
summary: Lots of new features.
---

The Pitivi team proudly presents Pitivi version **2020.09** "Hocus focus", a new milestone towards the most reliable video editor.

New features
------------

Since the 2014 fundraiser and until Pitivi 0.999 released in 2018, we included only critical features to focus on quality. For example, the proxy functionality which allows precise editing by seamlessly using the optimized media when the original media format is not supported officially.

Life and GSoC happens and we developed many good features over this period. Most of these originate from the Google Summer of Code program in which we took part, but not only. The new features came with accompanying unit tests and they have been merged only after careful code reviews. Even so, we kept them in the development branch.

This approach led to extra work for taking care of two branches. In addition to a "stable" branch out of which we were making releases, we also maintained a "development" branch in which we were merging cool features. Luckily, despite not much appearing to be happening with the project due to releases containing only bug fixes, contributors kept showing up, and not only because of GSoC.

Since the previous release we came to our senses and reconsidered the earlier decision. Pitivi 2020.09 includes a ridiculous number of new features, for your delight. Read the full list below and get ready to be blown away by what our contributors built.

Quality
-------

The user survey we conducted in 2013 revealed the most important point was to have a “stable as hell” basic editor. Since the previous release two years ago, we fixed the last outstanding bugs in [GStreamer Editing Services](https://gstreamer.freedesktop.org/documentation/gst-editing-services/index.html) (GES), Pitivi's reusable video editing backend.

At this point, the extensive GES and Pitivi unit tests and the gst-validate testing framework developed for GStreamer/GES allow us to make changes without being too afraid of introducing regressions. Thanks to the quality assurance we have in place, we are now switching to a date-based versioning scheme for Pitivi. The intention is to release often what we have at that point and make gradual changes.

Even though there won't be a Pitivi 1.0, after 15 years of hard work we are proud GES has reached the "1.0" level. We are very happy to finally reach this essential milestone in the project life!

This was possible thanks to contributions from [Igalia](https://www.igalia.com/) who has been sponsoring the development of the GStreamer Editing Services by stabilizing it and implementing many features such as the integration with [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/) from Pixar, implementing timeline nesting, and clip speed control (yet to be leveraged in Pitivi).

We'll be looking for ways of creating a constant stream of money to fund development by drawing interest from institutions backed by public money. Until this will happen, if ever, we'll play with occasional fundraisers for improving stability. More about this in a separate blog post.

What's new
----------

This release includes lots of new features, originating from GSoC internships spanning four years, from University of Nebraska-Lincoln's SOFT-261 class Spring 2020 and from individual contributors.

#### GSoC 2017

*   A plugin system allows extending the Pitivi functionality medium-term, targeted for teams of editors.
*   A developer console plugin allows interacting with the project in a Python console.
*   Mechanism for supporting custom UI for effects issues of the automatically generated UI.
*   Custom UI for the frei0r-filter-3-point-color-balance and alpha effects.
*   Easy Ken-Burns effect.

#### GSoC 2018

*   The new Greeter perspective replaces the Welcome wizard dialog and allows a slick selection of a recently opened project.
*   When being resized, the Viewer shows the percent of the actual widget size in relation to the project video size.
*   The Viewer size snaps at 50% when being resized.
*   Scaled proxies if optimized media is too much for your machine.

#### GSoC 2019

*   Timeline markers.
*   Support for nested timelines by importing entire XGES files as a clip.
*   The Effects Library has been redesigned to allow quick access to effects.
*   Ability to favorite effects in the Effects Library.
*   Improved workflow for adding effects.
*   The refreshed clip effects UI allows working on multiple effects at a time.

#### GSoC 2020

*   Refactored Media Library to use the same logic for the different view modes.
*   Refactored Render Dialog UI to avoid overwhelming people. Tell us what you think about it.

#### University of Nebraska-Lincoln's SOFT-261 class, Spring 2020

*   Restoring the editing state when reopening a project.
*   Safe areas visualization in the Viewer.
*   Easy alignment for video clips.
*   Composition guidelines in the Viewer.
*   Solid color clips.
*   Ability to mute an entire layer.

#### Individual hackers

*   Custom UI for the frei0r-filter-alphaspot effect.
*   Interactive Intro for newcomers to get familiar with the UI elements.
*   The action search is a shortcut to everything possible in the timeline, if you can find it. Press "/".
*   Ability to hide an entire layer.
*   New keyboard shortcuts for pros.

Under the hood
--------------

Since the Pitivi 0.15.2 release that came at the time of our 2014 crowdfunding campaign, we have basically changed everything under the hood, with too many bug fixes to count:

*   We rewrote the timeline canvas twice due to the changing technological landscape around us.
*   We've redone the clip transformation UI in the viewer twice.
*   We've redone the undo-redo system.
*   We've redone the Media Library.
*   We've redone the Effects Library.

Please see the corresponding [2020.09 milestone](https://gitlab.gnome.org/GNOME/pitivi/-/milestones/1) for details.

Download
--------

You can install Pitivi 2020.09 right away with flatpak. Follow the [installation instructions](https://flathub.org/apps/details/org.pitivi.Pitivi) from flathub.

What's next
-----------

We keep the highest priority issues in the [2020.12 milestone](https://gitlab.gnome.org/GNOME/pitivi/-/milestones/7). Basically a mix of bugfixing, cleanup and features.

Thanks
------

Many thanks to all the contributors!