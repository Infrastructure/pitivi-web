---
title: Pitivi 2022.06 — Reel Easing
date: Tue, 28 Jun 2022 08:28:00 +0000
tags:
  - Release
summary: Object tracking, beat detection and Clip Properties enhancements.
---

The Pitivi team proudly presents Pitivi version **2022.06** "Reel easing".

What's new
----------

This release includes two large features originating from GSoC internships. The continued collaboration with the University of Nebraska-Lincoln's SOFT-261 resulted in a handful of very cool and useful features. Multiple occasional individual contributors made awesome enhancements and bug-fixes.

Note a possibly disruptive change in the click timeline behavior. Now, by default left-click is for selecting and dragging clips and right-click for seeking or scrubbing. If you only use a single mouse button, enable the previous combined select-and-seek by left-clicking behavior in the preferences.

Quality
-------

Sensible unit-tests have been added to prevent the 20+ fixed bugs to reappear.

New features
------------

Vivek R contributed during GSoC 2020 the initial object tracking functionality, based on [OpenCV](https://opencv.org). The UI and logic have been further enhanced to allow verifying and correcting frame by frame the positions provided by the tracker. For now, the tracked object can be covered by a colored rectangle. This allows for example to hide faces, licence plates, etc. or just as well put a bright colored rectangle on a moving spot.

The tracking logic interfacing with OpenCV has actually been developed as part of GStreamer, so everybody can use it. You're welcome.

{{< figure src="object-tracker-perspective.png" title="Pitivi object tracker" >}}

Piotrek Brzeziński integrated [librosa](https://librosa.org/) during GSoC 2021 to allow detecting music beats. The beats are displayed as markers on the clip. The other clips can be easily arranged to music as they will snap at these markers when dragged.

{{< figure src="beat-detection-and-vu-meter.png" title="Pitivi beat detection. Pitivi VU meter. Sneak peek at clip speed control." >}}

For some reason we failed to merge earlier the playback sound VU meter developed by a team from University of Nebraska-Lincoln's SOFT-261 course two years ago. The VU meter resides currently at the right of the viewer, see the screenshot above.

Other teams from University of Nebraska-Lincoln's SOFT-261 course spring 2021 contributed features visible in the Clip Properties panel. Some involved enhancements in Pitivi's GStreamer Editing Services backend library.
 - Title clip text border and shadow are not forced anymore and can be controlled. 
 - The aspect-ratio can be maintained when resizing clips.
 - The Source blending mode can be selected instead of Over. More blending modes to come in time.
 - Fading in or fading out clips is now easier with new controls. For finer control one can still interact directly with the keyframes line on the clip itself.

{{< figure src="clip-properties-enhancements.png" title="Pitivi clip properties enhancements" >}}

Now it's possible to cut selected clips to paste them at a different position.

Under the hood
--------------

Note both the object tracking and beat detection depend on 3rd party libraries, namely OpenCV_contrib and librosa. They need to be installed on your system for the features to be presented.

Please see the corresponding [2022.06 milestone](https://gitlab.gnome.org/GNOME/pitivi/-/milestones/9) for details about the changes that went into this release, including the full list of bugs which have been fixed.

Download
--------

You should be able to install Pitivi 2022.06 in the next week or two with flatpak. We'll tweet about it when it's ready.

What's next
-----------

Object tracking and beat detection pave the way towards more advanced features. Come to our chat room and tell us further developments you'd like!

We're in the process of merging the zoom-fitted timeline which will allow quick clip operations over the entire timeline.

Hopefully soon we'll fix the last issues with clip speed control and we can enable the feature, until then it remains hidden. See how it looks in one of the screenshots above.

Despite missing the deadline for registering as a standalone org in this year's Google Summer of Code, we did find a place under GNOME's umbrella. This summer we're focusing on polishing the timeline and on porting Pitivi to GTK 4.

Once we're done with GSoC we'll look into merging this year's UNL SOFT-261 contributions.

Thanks
------

Many thanks to all the contributors and translators! Not the last to the librosa and OpenCV developers.
