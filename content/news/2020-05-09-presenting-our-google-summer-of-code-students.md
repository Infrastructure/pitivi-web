---
title: 'Presenting our Google Summer of Code students!'
date: Sat, 09 May 2020 07:00:38 +0000
tags:
  - GSoC
summary: Welcome to Abhishek, Ayush and Vivek who join Pitivi as part of the Google Summer of Code.
---

Google has published the list of students accepted in the [Google Summer of Code](https://summerofcode.withgoogle.com) program. The accepted students work on open-source software. Pending monthly evaluations, the students receive a stipend from Google. Like [last year](https://developer.pitivi.org/Past_GSoCs.html), we're mentoring [three students](https://summerofcode.withgoogle.com/organizations/5677249036025856/)! 

[Abhishek Kumar Singh](https://gitlab.gnome.org/gaharavara)  will improve the Media Library. The current implementation will be initially simplified to deal with a single Gtk.FlowBox container. Asset tagging will provide the info to display in a new Folder View. The stretch goal is to display the assets by date. 

[Ayush Mittal](https://gitlab.gnome.org/ayush9398) will improve the Render experience. The current UI will be simplified, allowing the user to select a meaningful preset. Specifying the export quality for the [officially supported encoders](https://gitlab.gnome.org/GNOME/pitivi/-/blob/413abf1f83bdac7d70863b91d820962b39cb9cac/pitivi/render.py#L89) will be possible using a slider widget. The advanced options will still be available but not directly visible. 

[Vivek R](https://gitlab.gnome.org/123vivekr) will implement face/object tracking and blurring. A new GStreamer plugin will allow tracking a specified region in the video using OpenCV. The obtained tracking information is presented to the user to be reviewed and adjusted in a new UI Perspective. The user can apply the adjusted positions to a blur effect applied to the clip.

Please get in touch if you are interested in working with us. You can find us in [https://matrix.to/#/#pitivi:matrix.org](https://matrix.to/#/#pitivi:matrix.org)