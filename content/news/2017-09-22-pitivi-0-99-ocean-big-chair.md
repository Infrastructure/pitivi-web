---
title: 'Pitivi 1.0 Release Candidate — "Ocean Big Chair"'
date: Fri, 22 Sep 2017 11:40:35 +0000
tags:
  - Release
---

We’re proud to release the first Pitivi 1.0 release candidate “Ocean Big Chair” (0.99). This release has many bug fixes and performance improvements, and is a release candidate for 1.0. Our test suite grew considerably, from 164 to 191 meaningful unit tests.

You can [install it](https://developer.pitivi.org/Install_with_flatpak.html) right away using Flatpak.

### GSoC

Early on this year, we got caught up in the Google Summer of Code. A lot of students hacked on Pitivi this spring, to get to know Pitivi better. As a result quite a few important fixes and improvements have been made. A big thank you to all of the students who contributed!

This summer we had three GSoC Pitivi projects. The GSoC work has been merged on the “master” branch and we made a separate “1.0” branch where we implement or backport the relevant fixes. A special thank you to Suhas Nayak and Ștefan-Adrian Popa, two of our GSoC students who contributed a large number of bugfixes and made possible this release.

### 1.0

As you might know, we're focused on bug fixing until Pitivi 1.0. See what’s left to do for 1.0 in [Phabricator](https://phabricator.freedesktop.org/project/view/125/).

We need people to test Pitivi (the Flatpak "stable" branch) and report back any crash or stability issue they notice, so we fix it. — If you want to [help](https://www.pitivi.org/contributing/) in any way, come to our [IRC channel](https://www.pitivi.org/contact/).