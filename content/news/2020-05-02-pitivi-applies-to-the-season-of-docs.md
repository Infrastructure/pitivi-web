---
title: 'Pitivi applies to the Season of Docs'
date: Sat, 02 May 2020 12:40:20 +0000
tags:
  - Season of Docs
---

The Pitivi video editor is [based](https://developer.pitivi.org/GES.html) on the [GStreamer Editing Services](https://gstreamer.freedesktop.org/documentation/gst-editing-services/index.html) library (GES). Various projects use GES to manage audio or video projects and export the project to a new file to be distributed.

Pitivi is developed in very close contact with GES. Both Pitivi and GES would benefit a lot from better documentation.

We’re applying to the [Season of Docs](https://developers.google.com/season-of-docs/) program, where Google pays technical writers to contribute to open-source projects. Check out the [technical writer guide](https://developers.google.com/season-of-docs/docs/tech-writer-guide) for details and the program timeline. Read below the project ideas if you are interested in working with us!

**UPDATE:** Pitivi is taking part in the Season of Docs under GNOME's umbrella. If interested to update Pitivi's user manual check the [Update app help](https://www.gnome.org/news/2020/05/gnome-season-of-docs-2020/) project idea and see below for details. ![](/i/seasonofdocs_logo_secondarygrey.png)

Project ideas
-------------

### GES: Write overviews and clarify the API reference

What GES is missing the most is a set of easy to understand high-level **overviews** for newcomers. You could write for example overviews about:

*   Assets and proxy management in a project
*   Timeline composed of Layers, and the output a/v tracks
*   Layers composed of various Clip types, and how these can be created
*   Effects which can be applied to Clips, and controllable properties

While preparing the high-level overviews you’ll dive into the **API reference**. The current API documentation is usable, but is not in the best shape. As you notice gaps, unclear sentences, missing details, you would either fix them on the spot or keep track of them so they can be fixed. The entire API reference can then be reviewed and refreshed in this second step. A stretch goal would be writing one or more **tutorials**, to exemplify easy video tasks which can be automated. The base code for these would be provided by us, and be most probably written in Python:

*   Cutting every third second of a video to produce a shorter video with skips,
*   Creating a photos slideshow with random transitions between the photos,
*   Displaying multiple videos at the same time in mosaics,
*   Animating a title clip in the center of the image from 0% to full size.

The GES overviews and tutorials would be kept in the [GES git repository](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/docs) as markdown files. The API is documented directly in the [source code](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/ges) written in C.

To get a better idea about the GES API, check the [hierarchy of classes](http://lazka.github.io/pgi-docs/#GES-1.0/hierarchy.html).

The current [GES documentation](https://gstreamer.freedesktop.org/documentation/gst-editing-services/index.html) is integrated into the GStreamer documentation. The structure is specified in [docs/sitemap.txt](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/blob/master/docs/sitemap.txt), and the HTML is generated with [Hotdoc](https://hotdoc.github.io) out of the C files and [other markdown bits](https://gitlab.freedesktop.org/gstreamer/gst-editing-services/tree/master/docs).

### Pitivi: Update the user manual

The user manual is available both [online](https://www.pitivi.org/manual/) and in Pitivi by pressing F1. The latest Pitivi features should be documented in the user manual and the current content should be brought up to date. The user manual is kept in the [Pitivi git repository](https://gitlab.gnome.org/GNOME/pitivi/-/tree/master/help/C) as [mallard](http://projectmallard.org/about/learn/tenminutes.html) files.

How to contact us
-----------------

If interested, get in touch with us [on Matrix](https://matrix.to/#/#pitivi:matrix.org), so we can prepare. You are expected to ask a lot of questions.