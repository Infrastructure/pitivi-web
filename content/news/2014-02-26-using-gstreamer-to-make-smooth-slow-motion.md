---
title: 'Using GStreamer to make smooth slow motion!'
date: Wed, 26 Feb 2014 21:29:00 +0000
---

This is a very good example of what our developers can do! There has been some preliminary work on bringing slow and fast motion to GStreamer and Pitivi, and a plugin has been created to allow for frame interpolation, which means you and I with our regular 24 frames per second cameras will be able to get **smooth slowmotion** from Pitivi in the future!

All that work has not yet been merged and thoroughly tested, and we need your [help](http://fundraiser.pitivi.org/donate) to make it happen!

To help you understand the difference between regular and smooth slowmotion, here is a video showing both types side by side, created by Alexandre Prokoudine. The difference is quite stunning!

Thanks to [Alexandre Prokoudine](http://vimeo.com/prokoudine) for the "Slowmo video effect with GStreamer" video below:

{{< vimeo 76288762 >}}
