---
title: "Tour"
---

<section class="centered-introduction">
<h1>Pitivi is a truly unique video editor.</h1>
<ul>
    <li>With its "no downstream hacks" and "upstream first" approach, it acts as a cutting-edge catalyst to push <a href="https://developer.pitivi.org/Architecture.html">open-source technologies we love</a> forward.</li>

    <li>It champions design and usability research: there is no eternal newbie, and Pitivi's user interface is carefully designed to suit both the newcomer <em>and</em> the professional, to be efficient <em>and</em> intuitive. We adhere to the <a href="https://developer.gnome.org/hig/stable/" title="GNOME's HIG documentation">GNOME Human Interface Guidelines</a> and regularly discuss with artists all around the planet to tackle the hard problems the right way, to make complex tasks easy to accomplish.</li>

    <li>It is a vibrant community-driven project, backed by longstanding contributors with a strong interest and experience in multimedia.</li>
<ul>
</section>


<section>
<a href="/i/screenshots/2020.05.png" class="discreet">
    <h2>Stunning elegance<img src="/i/screenshots/2020.05.png" alt="Main window" style="width: 100%;"></h2>
</a>
</section>


<section class="auto-column-half">
<h2>Free and Open Source</h2>
<p>Pitivi respects your freedom and keeps getting better with each new release. It will never require licensing fees or use <a href="https://en.wikipedia.org/wiki/Digital_rights_management" title="Wikipedia: Digital Rights Management">DRM</a> to prevent you from using it whenever, wherever and however you like. Developed collaboratively by people from all around the world, its sole purpose is to be a fantastic video editor to empower people to express themselves through video. Like GStreamer and <a href="https://developer.pitivi.org/GES.html" title="GStreamer Editing Services">GES</a>, Pitivi is distributed under <a href="https://www.gnu.org/copyleft/lesser.html" title="GNU Lesser General Public License">LGPL</a>.</p>

<h2>Anything in</h2>
<p>As long as it is well supported by the <a href="https://en.wikipedia.org/wiki/GStreamer" title="Wikipedia: GStreamer">GStreamer Multimedia Framework</a>, Pitivi will accept any format you throw at it.</p>
<p>Some video editing applications only allow you to use footage that has the exact same framerate inside a project. Well, our timeline is like the honey badger: <em>it doesn't care</em>.</p>

<!-- TODO: when our timeline is a bit more touch-friendly...
<h2>Something you can touch and feel</h2>
<img src="https://placebear.com/720/360" />
<p>Our new user interface is compatible with touchscreen devices. Yes, you read that right.</p>
-->

<h2>Hundreds of animated effects, transitions and filters</h2>
<p>Not only do you have access to over 70 industry-standard transitions and more than a hundred video and audio effects, you can add true dynamism to your films by animating all the effects' properties over time with keyframes.</p>

<h2>Flexible and efficient</h2>
<p>Packed with time-saving features such as realtime trimming previews, ripple and roll editing, grouping and snapping, realtime assets management and searching, playhead-centric zooming and editing, non-modal cutting, detachable interface components, smooth scrolling, automatic zoom adjustment, Pitivi allows you to cut with ease. Like a ninja with a vibroblade.</p>

<h2>Beautiful audio waveforms</h2>
<p>Visuals are only a tiny part of the story. A movie without balanced and well-timed sound is amateur at best. Pitivi provides you with a clear and accurate representation of loudness, so you can balance things out or easily detect patterns for beat matching, clap synchronization or simply cutting to the music.</p>

<h2>Speaks many languages</h2>
<p>Thanks to the tireless work of the GNOME translation and localization teams, Pitivi is available in <a href="https://l10n.gnome.org/module/pitivi/">many languages</a>. Your language is not listed there? <a href="https://wiki.gnome.org/TranslationProject">Make it happen</a>.</p>
</section>


<section class="auto-column-half">
<h2>Easy to learn. Exciting to master</h2>
<p>You have better things to do than dealing with cumbersome user interfaces or reading through books just to get started. Resulting from years of experience and analysis, our user interface is <strong>intuitive</strong> and <strong>self-documenting</strong>: it provides you with all the contextual hints you need to grasp the essentials without needing to dive into the user manual all the time. But if you want to do something more complicated or want to better understand the tenets of our design, our fully-searchable, topic-oriented and multilingual user manual is right there, at your fingertips.</p>

<h2>It grows on you and grows with you</h2>
<p>In addition to providing you with the best video editing software possible, nothing is more important to us than encouraging you to adapt and improve Pitivi and <a href="https://developer.pitivi.org/GES.html" title="GStreamer Editing Services">GES</a>. After all, you should truly <em>own</em> your software. The reason we can keep improving with each release is because <em>you</em> <a href="/contribute/">get involved</a>.</p>

<h2>Natural desktop environment integration</h2>
<p>Pitivi integrates best with the <a href="https://gnome.org">GNOME</a> desktop — or any Linux desktop environment, really. We don't just look pretty alongside other GTK applications though: we help redefine the desktop experience by working closely with toolkit and theme designers to suit our needs and push the whole ecosystem forward.</p>

<h2>Automatic project backups that actually work</h2>
<p>Once created, your project is always safe. With Pitivi, you can even cut the power or crash the entire computer and Pitivi will offer you to restore from the last automatic checkpoint. Best of all: Pitivi won't try to second-guess you. We've seen enough examples out there of applications that can't be trusted to handle backups <em>correctly</em>.</p>

<h2>Background processing</h2>
<p>Pitivi generates thumbnails, waveforms and clip proxies in the background, without disrupting your workflow.</p>

<h2>No need to reinvent the wheel</h2>
<p>With the <a href="https://developer.pitivi.org/GES.html">GStreamer Editing Services</a> library, we are laying solid foundations for the future based upon over a decade of experience. Built atop the <a href="https://www.openhub.net/p/gstreamer">giant</a> shoulders of the industry-standard GStreamer multimedia framework, GES reduces fragmentation and risk while allowing diversity for application writers and adaptability for all kinds of purposes.</p>
</section>

<section style="clear: both;">
<div>
<h2>See what others have to say about it!</h2>
<p>Take a look at some of the <a href="https://developer.pitivi.org/Praise.html">praises/positive comments</a> we have received so far.</p>
</div>
</section>


<section style="clear: both;">
<h1>What, you wanted a boring list of features?</h1>
<div class="auto-text-columns">
<ul>
    <li>Unlimited video/audio track layers</li>
    <li>Full undo/redo history</li>
    <li>Frame stepping, keyboard controls and shortcuts</li>
    <li>Trimming, splitting/cutting</li>
    <li>Snapping</li>
    <li>Ripple edits/roll edits</li>
    <li>Sound mixing of multiple concurrent audio layers</li>
    <li>Volume keyframe curves</li>
    <li>Keyframable audio effects</li>
    <li>Audio waveforms</li>
    <li>Keyframable video effects</li>
    <li>Opacity keyframe curves</li>
    <li>Video thumbnails with two-stage caching</li>
    <li>SMPTE video transitions</li>
    <li>Ability to specify custom aspect ratios and framerates</li>
    <li>Ability to create presets for project settings and rendering</li>
    <li>Greeter that helps you start a project or load recent projects quickly</li>
    <li>Ability to preview video, audio and image files directly in the file chooser before importing</li>
    <li>Fast, playhead-centered zooming</li>
    <li>Mousewheel integration with modifier keys for timeline navigation</li>
    <li>Scrubbing</li>
    <li>Grouping and ungrouping of clips</li>
    <li>Importing support for all video/audio/image formats provided by GStreamer plugins. Pitivi was the first open source video editor to support the <a href="https://en.wikipedia.org/wiki/Material_Exchange_Format">Material eXchange Format</a> for professional digital video and audio media.</li>
    <li>Rendering in any container and codec properly supported by GStreamer plugins</li>
    <li>Nanosecond precision</li>
    <li>Accepts both timecodes and frames</li>
    <li>Automated project backups</li>
    <li>Tarball export</li>
    <li>Commandline processing and rendering mode using GES</li>
    <li>Scripting capabilities using GES</li>
    <li>Plugins</li>
    <li>Developer console</li>
    <li>Written in Python, with a clean and modular codebase</li>
    <li>Hardware-accelerated and touch-capable user interface</li>
</ul>
</div>
<p>A must-have feature is missing? You can certainly <a href="/contribute/">help us implement it</a>.</p>
</section>
