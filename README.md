# Pitivi's Official Website

The source for the [Pitivi website](https://www.pitivi.org) is maintained using
[Hugo](https://gohugo.io), a Static Site Generator. The site uses the following
types of files for the content generation:

-   HTML files with extension `.html`
-   Markdown files with extension `.md`

## Project Structure

Hugo tutorials advertise how to use a theme, but we don't use any theme.
As such, the building blocks of the pages can be found in `layouts/`.
The content to be presented can be found in `content/`.

The tree below gives a picture of the [directory structure].

    ...
    ├── content
    │   ├── tour.html                         # The /tour page
    │   ├── donators
    │   │   ├── index.html                    # The content of the /donators page
    │   │   └── shut-up-and-take-my-money.png # PNG used by the /donators page
    │   └── news                              # The "news" section
    │       └── *.md                          # Blog posts "/news/*.html"
    ├── layouts
    │   ├── _default
    │   │   ├── baseof.html                   # Template for *all* the pages
    │   │   ├── list.html                     # Template for the content of the list pages of sections
    │   │   └── single.html                   # Template for the content of normal pages such as /tour
    │   ├── partials                          # Partials used in the templates above
    │   │   ├── footer.html                   # The bottom of the pages
    │   │   ├── head.html                     # The <head> of the pages
    │   │   └── header.html                   # The top of the pages
    │   ├── shortcodes                        # Small parts ready to include in any page
    │   │   └── videos_site.html
    │   └── index.html                        # The content of the top page
    ├── static                                # Static content
    │   ├── i                                 # Stuff
    │   └── css
    │       └── pitivi.css                    # Stylesheet used in all the pages
    ├── .gitlab-ci.yml                        # Pipelines for building and deploying the website
    └── config.toml                           # Site configuration including the top menu items


## Contributing

We always welcome people who want to contribute towards our project. For
suitable information on how can you contribute to the website, on how to
report bugs, on how to request new features or anything that can make the
website a better experience for the end users, please [get in
touch](https://www.pitivi.org/contact/).

## Setup the website locally

To get the site up and running locally, follow the below steps:

1. Install [Hugo](https://gohugo.io/getting-started/installing/).
2. Create a local clone of the website:

```
git clone https://gitlab.gnome.org/Infrastructure/pitivi-web.git
```

3. Enter into the pitivi-web directory:

```
cd pitivi-web
```

4. Build the site and make it available:

```
$ hugo server --buildDrafts --buildFuture
```

5. Browse to [http://localhost:1313](http://localhost:1313) to view the
website.

6. Edit `layouts/index.html` or any other file, save the changes and notice
the corresponding page loaded in the browser refreshes itself.

## User manual

TODO: How to update the user manual.

## Pipeline

The pipeline used by the website is the top-level component of continuous
integration, delivery, and deployment.

The pipeline defined by the GTK.org uses the `Ruby2.5` image. The pipeline
consists of a script that runs before the site is tested/deployed. The
script that runs before the test/deployment of the website basically
installs all the `gem/npm dependencies`, fetches the API data regarding the
GTK from its [gitlab instance][gtk-gitlab] and then structurizes the website
before testing/deploying.

`test` stage is performed on all branches but `master`. `deploy` stage on
the other hand is performed only on `master` branch.

## License Information

pitivi.org is licensed under the [Creative Commons
BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/).
